#!/usr/bin/python

import sys
from anytree import Node, RenderTree
from anytree.exporter import DotExporter
import numpy as np
import itertools
import random
import multiprocessing
import time
import os
import copy

class Crawler(object):
	def __init__(self, tablica):
		'''
		tablica to plansza do gry czyli obiekt klasy board po ktorym 'porusza' sie crawler
		'''
		self.x, self.y, self.rozmiar=tablica.return_dim()
		self.tablica=tablica
	
	def position_crawler(self,x=0,y=0):
		''' 
		ok ustawmy crawlera z defaultu bedzie na pozycji 0,0
		'''
		self.pos=[x,y]
		self.path=[self.tablica[self.pos]]
		return self

	def show_current_position(self):
		return self.pos

	def show_path(self):
		return ",".join(self.path)

	def crawl(self, step):
		'''
		path is an element from 1:4, representing directions on a grid 1-go left, 2 go up, 3 go rigt 4 go down
		'''
		if 1 == step:
			self.pos=[self.pos[0],self.pos[1]-1]
		elif 2 == step:
			self.pos=[self.pos[0]-1,self.pos[1]]
		elif 3 == step:
			self.pos=[self.pos[0],self.pos[1]+1]
		elif 4 == step:
			self.pos=[self.pos[0]+1,self.pos[1]]

		if self.pos[0] < 0 or self.pos[1] < 0  or  self.pos[0] >= self.x  or  self.pos[1] >= self.y: 
			self.path.append('xx')
		else:
			self.path.append(self.tablica[self.pos])
		
		return self

	def evaluate_path(self):
		'''
		k mamy sciezke teraz warunki konca
		'''
		if 'xx' in self.path: # nie wpadamy na black pola i nie wychodzimy poza mape
			return False,False
		if len(self.path) == self.rozmiar+1: #przeszlismy przez wszystkie biale pola
			if self.path[-1] == self.path[0]: # doszlismy z poczatku do konca
				return True,True
			else:
				
				return False,False
		else: # nie wypelnilismy wszystkich bialych pol
			if len(set(self.path)) == len(self.path): # ale nie crossujmy sie z niczym wczesniej
				return True,False
			else:
				return False, False
		#print 'Przeszedlem wszystkie if'
		#return 'Przeszedlem wszystkie ify'
					
class Board(object):
	def __init__(self, x,y, black=[]):
		'''
		x,y to rozmiary tablicy w systemie 1-kowym, black to tablica tablic tez w systemie 1-kowym
		'''
		self.x=x
		self.y=y
		self.czarne=[[pole[0]-1, pole[1]-1] for pole in black]	
		self.plansza=[]
	
	def create_board(self):
		for x in itertools.product([x for x in range(0,self.x)], [x for x in range(0,self.y)]):
			self.plansza.append("".join(map(str,x)))
		self.plansza=np.reshape(self.plansza,(self.x,self.y))
		for x,y in self.czarne:
			self.plansza[x,y]='xx'
		return self
	def print_board(self):
		print self.plansza
		return True

	def __getitem__(self,x):
		#x=[x[0]-1,x[1]-1]
		return self.plansza[x[0],x[1]]
		
	def return_dim(self):
		return self.x, self.y, int(self.x)*int(self.y)-len(self.czarne)

def add_branch(parent,level):
        dodane_poziomy=[]
        for droga in range(1,5):
                nazwa=str(droga)+"_"+str(random.randint(1,1000000))+"_"+str(level)
                nazwa=Node(nazwa, parent=parent)
                dodane_poziomy.append(nazwa)
        return dodane_poziomy


def inner_loop(parent,level,plansza):
	good_leaf=[]
	dodano=add_branch(parent,level)
	koniec=0
	for lisc in dodano:
		droga=[]
		for x in str(lisc).split("/"):
                	try:
                        	droga.append(int(x.split("_")[0]))
                        except:
                        	pass
		crawler1=Crawler(plansza)
                crawler1.position_crawler(0,0)
                [crawler1.crawl(krok) for krok in droga]
		if crawler1.evaluate_path()[0]:
                      	good_leaf.append(lisc)
		if crawler1.evaluate_path()[1]:
                      	print 'wybrana sciezke %s' % (",".join(map(str,droga)))
			koniec=1
	return good_leaf,koniec

def log(wynik):
	global ocenione
	global end
	ocenione.extend(wynik[0])
	end=wynik[1]


if __name__=='__main__':
	plansza=Board(5,5,[[1,1],[5,5]])
	plansza.create_board()
	plansza.print_board()
	print plansza[0,0]

	plansza2=Board(3,3,[[2,2]])
	plansza2.create_board()
	plansza2.print_board()
	print plansza2[0,0]

	plansza3=Board(10,10,[[1,3],[1,7],[4,1],[5,4],[5,9],[6,5],[7,1],[7,6],[8,7],[9,3],[9,8],[10,1],[10,6],[10,10]])
	plansza3.create_board()
	plansza3.print_board()

	plansza4=Board(5,5,[[1,3],[4,3],[5,1]])
	plansza4.create_board()
	plansza4.print_board()	

	root=Node('root') # to odpowiada pozycji startowej 
        ocenione=root
	dodano=add_branch(ocenione,1)
	ocenione=[]	
	for lisc in dodano:
		droga=[int(x.split("_")[0]) for x in lisc.name.split("/")]
		#print droga
		crawler1=Crawler(plansza3)
	        crawler1.position_crawler(0,0)
		for krok in droga:
	                crawler1.crawl(krok)
		if crawler1.evaluate_path()[0]:
			ocenione.append(lisc)
	end=0
	for level in range(100):
		ocenione_copy=copy.deepcopy(ocenione)
                ocenione=[]
                print level,end
		#print ocenione_copy
                pool = multiprocessing.Pool(processes=6)
		for parent in ocenione_copy:
			pool.apply_async(inner_loop, args=(parent,level,plansza3), callback=log)
		pool.close()
		pool.join()
		if end:
			break



'''
print 'TEST 1-------------------'
        sciezka1=[3,3,4,4,2,2,1,1]
        print sciezka1
        crawler1=Crawler(plansza2)
        crawler1.position_crawler(0,0)
        for krok in sciezka1:
                crawler1.crawl(krok)
        print crawler1.show_path()
        print crawler1.evaluate_path()

        print 'TEST 2------------------'
        sciezka1=[3,3,3]
        print sciezka1
        crawler1=Crawler(plansza2)
        crawler1.position_crawler(0,0)
        for krok in sciezka1:
                crawler1.crawl(krok)
        print crawler1.show_path()
        print crawler1.evaluate_path()

        print 'TEST 3 ------------------'

        sciezka1=[3,4]
        print sciezka1
        crawler1=Crawler(plansza2)
        crawler1.position_crawler(0,0)
        for krok in sciezka1:
                crawler1.crawl(krok)
        print crawler1.show_path()
        print crawler1.evaluate_path()

        print 'TEST 4 ------------------'

        sciezka1=[3,3,4,4,1,1,2,2]
        print sciezka1
        crawler1=Crawler(plansza2)
        crawler1.position_crawler(0,0)
        for krok in sciezka1:
                crawler1.crawl(krok)
        print crawler1.show_path()
        print crawler1.evaluate_path()

        sciezka1=[3, 1]
        print sciezka1
        crawler1=Crawler(plansza2)
        crawler1.position_crawler(0,0)
        for krok in sciezka1:
                crawler1.crawl(krok)
        print crawler1.show_path()
        print crawler1.evaluate_path()
'''
